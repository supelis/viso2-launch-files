# README #

* Viso2 launch files
* Version 0.1

### How do I get set up? ###

* Create and prepare catkin workspace if you haven`t already ([ROS Tutorial](http://wiki.ros.org/catkin/Tutorials/create_a_workspace))
* Clone [viso2_ros](https://github.com/srv/viso2) source code into your workspace/src folder
```
#!bash
git clone https://github.com/srv/viso2

```

* Catkin make your workspace
* Clone this repository insidde your workspace/src directory

```
#!bash
git clone https://bitbucket.org/supelis/viso2-launch-files viso2_launch

```

* Install required dependancies
```
#!bash
rosdep install viso2_launch

```

* Calibrate your camera using [this tutorial](http://wiki.ros.org/camera_calibration/Tutorials/MonocularCalibration)
* After succesfull calibration, set calibration file path in line 8 of `launch/mono.launch` file (camera_info_url)
* Execute command 

```
#!bash

roslaunch viso2_launch mono.launch
```
* If no errors occured you should see position updates in `/mono_odometer/pose` topic.
* Feel free to change parameters inside `launch/mono.launch` file.
